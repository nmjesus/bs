module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            js: {
                files: ['src/**/*.js', 'test/**/*.js'],
                tasks: ['jsh', 'test']
            },
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            src: ['src/**/*.js', 'test/**/*.js'],
        },
        mochacli: {
            options: {
                harmony: true,
                reporter: "nyan",
                ui: "tdd"
            },
            all: ["specs/server/*Spec.js"]
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-mocha-cli');

    grunt.registerTask('jsh', ['jshint']);
    grunt.registerTask('test', ['mochacli']);
    grunt.registerTask('w', ['watch']);
};

'use strict';

var Board = require('./board'),
    Piece = require('./piece'),
    Player = require('./player'),
    utils = require('./utils'),
    config = require('./config'); 

var Game = function() {
    this.init();
};

Game.prototype = {
    board: null,
    pieces: [],
    player: null,

    init: function() {
        var pieces = [], _cur;

        this.board = new Board(config.BOARD_SIZE);

        for(var k in config.PIECES_AVAILABLE) {
            for(var i = 0, len = config.PIECES_AVAILABLE[k].qty; i<len; i++) {
                _cur = config.PIECES_AVAILABLE[k];
                pieces.push(this.createPiece(_cur));
            }
        }

        this.pieces = this.putPieces(pieces, this.board);
        this.player = new Player(this.board, this);
    },

    createPiece: function(piececonf) {
        var p, coords = [
            utils.random(0, config.BOARD_SIZE),
            utils.random(0, config.BOARD_SIZE-1)
        ];
        if(config.DEBUG) {
            p = new Piece(piececonf.size, piececonf.orientation, piececonf.coords, piececonf.val);
        } else {
            p = new Piece(piececonf.size, utils.getRandom(config.ORIENTATION), coords, piececonf.val);
        }
        return p;
    },

    putPieces: function(pieces, board) {
        var added, _cur, _fPieces = [];
        for(var i = 0, len = pieces.length; i<len; i++) {
            added = false;
            _cur = pieces[i];
            while(!added) {
                pieces[i] = this.createPiece(_cur);
                added = board.putPiece(pieces[i]);
                if(config.DEBUG) {
                    added = true;
                }
            }
            _fPieces.push(pieces[i]);
        }
        return _fPieces;
    },

    fire: function(cmd) {
        var col = cmd[0],
            row = cmd[1],
            hitpoint = this.board.grid[row][col],
            boatSunk = false;

        if(hitpoint && hitpoint !== config.HITPOINTS.WATER) {
            this.board.grid[row][col] = config.HITPOINTS.HIT;
            boatSunk = this.destroyedAnyBoat([row, col], hitpoint);
        } else {
            this.board.grid[row][col] = config.HITPOINTS.MISS;
        }

        return {
            hitpoint: hitpoint,
            sunk: boatSunk,
            win: boatSunk ? this.areAllShipsSunk() : false
        };
    },

    areAllShipsSunk: function() {
        var win = false, _cur;
        var checkCoordsFN = function(coord) {
            return coord[0] === config.HITPOINTS.HIT && coord[1] === config.HITPOINTS.HIT;
        };
        for(var i = 0, len = this.pieces.length; i<len; i++) {
            _cur = this.pieces[i];
            win = _cur.coords.every(checkCoordsFN);
            if(!win) {
                return win;
            }
        }
        return win;
    },

    destroyedAnyBoat: function(coords, hitpoint) {
        var _cur, isSinking = true;
        for(var i = 0, len = this.pieces.length; i<len; i++) {
            _cur = this.pieces[i];
            if(_cur.sunk || _cur.val !== hitpoint) {
                continue;
            }
            isSinking = true;
            for(var j = 0, l2 = _cur.coords.length; j<l2; j++) {
                if(_cur.coords[j][0] === coords[0] && _cur.coords[j][1] === coords[1]) {
                    this.pieces[i].coords[j] = [config.HITPOINTS.HIT, config.HITPOINTS.HIT];
                }
                if(_cur.coords[j][0] !== config.HITPOINTS.HIT && _cur.coords[j][1] !== config.HITPOINTS.HIT) {
                    isSinking = false;
                }
            }
            if(isSinking) {
                this.pieces[i].sunk = true;
                return true;
            }
        }
        return false;
    }
};

module.exports = Game;

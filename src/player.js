'use strict';

var readline = require('readline'),
    utils = require('./utils'),
    config = require('./config');

var Player = function(board, game) { 
    this.board = board;
    this.game = game;
    this.init();
};

Player.prototype = {
    init: function() {
        this.motd();
        this.start();
    },

    start: function() {
        if(config.DEBUG || config.SHOW_GRID) {
            console.log(this.board.grid);
        }

        var iface = readline.createInterface(process.stdin, process.stdout);
            iface.setPrompt('target: ');
            iface.prompt();

        var _self = this;
        iface.on('line', function(input) {
            _self._handleCmd(input, iface);
        });
    },

    _handleCmd: function(input, iface) {
        var cmd = utils.translateInput(input);
        var valid = utils.isValidCmd(cmd),
            hp = {};
        if(!valid) {
            console.error('Wrong command. Usage: A1');
        } else {
            hp = this.game.fire(utils.convertCmd(cmd));
            this._handleHitpoint(hp);
        }
        if(!hp.win) {
            iface.prompt();
        } else {
            iface.close();
        }
    },

    _handleHitpoint: function(hp) {
        var kind;
        switch(hp.hitpoint) {
            case config.HITPOINTS.WATER:
                console.log('WATER');
                break;
            case config.HITPOINTS.MISS:
            case config.HITPOINTS.HIT:
                console.log('Already fired at that point');
                break;
            case config.HITPOINTS.BATTLESHIP:
            case config.HITPOINTS.DESTROYER:
                kind = hp.hitpoint === 1 ? 'battleship' : 'destroyer';
                console.log(['You hit', kind, (hp.sunk ? ' - sunk' : '')].join(' '));
                break;
        }
        if(hp.win) {
            console.log('Congrats! You won the match.');
        }
    },

    motd: function() {
        console.log('Hello Commander');
        console.log('It\'s time to sink some boats... get ready!');
    },

};

module.exports = Player;

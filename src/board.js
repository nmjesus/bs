'use strict';

var Board = function(d) {
    this.grid = this.create(d);
};


Board.prototype = {
    create: function(d) {
        var grid = [];
        for(var i = 0; i<d; i++) {
            grid[i] = [];
            for(var j = 0; j<d; j++) {
                grid[i][j] = 0;
            }
        }

        return grid;
    },


    hasAvailableSpace: function(piece) {
        var boardSize = this.grid.length;
        if((piece.coords[0] + piece.size > boardSize) && piece.orientation === 'horizontal') {
            return false;
        }

        if((piece.coords[1] + piece.size > boardSize) && piece.orientation === 'vertical') {
            return false;
        }

        return true;
    },


    putPiece: function(piece) {
        if(!this.hasAvailableSpace(piece)) {
            return false;
        }

        var coordsToSet = [];

        var row, pRow, col, pCol;

        if(piece.orientation === 'horizontal') {
            for(col = piece.coords[0]; col<(piece.coords[0] + piece.size); col++) {
                pRow = piece.coords[1];
                if(this.grid[pRow][col] === 0) {
                    coordsToSet.push({ row: pRow, col: col });
                } else {
                    return false;
                }
            }
        } else {
            for(row = piece.coords[1]; row<(piece.coords[1] + piece.size); row++) {
                pCol = piece.coords[0];
                if(this.grid[row][pCol] === 0) {
                    coordsToSet.push({ row: row, col: pCol });
                } else {
                    return false;
                }
            }
        }

        return this._addPiece(coordsToSet, piece);
    },

    _addPiece: function(coordsToSet, piece) {
        var i, _cur, res;
        piece.coords = [];

        for(i = 0; i<coordsToSet.length; i++) {
            _cur = coordsToSet[i];
            this.grid[_cur.row][_cur.col] = piece.val;
            res = piece;
            res.coords.push([_cur.row, _cur.col]);
            
        }
        return res;
    }
};


module.exports = Board;

'use strict';

const DEBUG = false;
const BATTLESHIP_SIZE = 5;
const DESTROYER_SIZE = 4;

var Config = {
    HITPOINTS: {
        WATER: 0,
        MISS: -1,
        HIT: -2,
        BATTLESHIP: 1,
        DESTROYER: 2
    },
    DEBUG: DEBUG,
    SHOW_GRID: false,
    BOARD_SIZE: 10,
    ORIENTATION: ['horizontal', 'vertical'],
    PIECES_AVAILABLE: {
        'battleship': {
            size: BATTLESHIP_SIZE,
            val: 1,
            qty: 1,
            // debug only
            //coords: [2, 6],
            //orientation: 'horizontal'
            //coords: [7, 4],
            //orientation: 'vertical'
            coords: [4, 5],
            orientation: 'vertical'
        },
        'destroyer': {
            size: DESTROYER_SIZE,
            val: 2,
            qty: DEBUG ? 1 : 2,
            // debug only
            //coords: [5, 3],
            //orientation: 'vertical'
            //coords: [7, 6],
            //orientation: 'vertical'
            coords: [3, 6],
            orientation: 'horizontal'
        }
    }
};

module.exports = Config;

'use strict';

var utils = {
    getRandom:  function(arr) {
        return arr[this.random(0, arr.length)];
    },

    random: function(min, max) {
        return Math.floor(Math.random() * (max-min) + min);
    },

    translateInput: function(str) {
        if(!str) {
            return false;
        }
        var input = str.toLowerCase().split(/([a-z])/i);
            input.shift();
        return [input[0], parseInt(input[1], 10)];
    },

    isValidCmd: function(cmd) {
        if(!cmd || cmd.length !== 2) {
            return false;
        }
        var col = cmd[0] && cmd[0].charCodeAt() >= 97 && cmd[0].charCodeAt() <= 106;
        var row = cmd[1] && cmd[1] >= 1 && cmd[1] <= 10;

        return !!(col && row);
    },

    convertCmd: function(cmd) {
        var charStartIdx= 97; // 'a'
        return [
            cmd[0].charCodeAt() - charStartIdx,
            cmd[1] - 1
        ];
    }
};

module.exports = utils;

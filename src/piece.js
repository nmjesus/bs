'use strict';

var Piece = function(size, orientation, coords, val) {
    this.size = size;
    this.orientation = orientation;
    this.coords = coords;
    this.val = val;
};

module.exports = Piece;

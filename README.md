## Battleship - Pebble {code} test ##

**Start:**
> git clone git@bitbucket.org:nmjesus/bs.git
> cd bs
> ./app.js

**For developers:**
> git clone git@bitbucket.org:nmjesus/bs.git
> cd bs
> npm install

**Grunt tasks available:**

 - watch - watch for file changes to run jshint and tests
 - jsh - run jshint
 - test - run tests
 
**Cli Arguments:**
> --show-grid - cheat code to display the grid and ships generated


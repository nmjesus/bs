#!/usr/bin/node --harmony

'use strict';

var Game = require('./src/game'),
    config = require('./src/config');

var args = process.argv.slice(2);
if(args.indexOf('--show-grid') !== -1) {
    config.SHOW_GRID = true;
}

new Game();

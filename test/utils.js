'use strict';

var expect = require('expect.js'),
    utils = require('../src/utils');

describe('Utils testing', function() {
    var numArr = [1,2,3,4,5,6,7,8,9,10],
        cmds = [
            'A11',
            '11',
            'A10',
            'j10',
            'a0',
            'a5',
            'j0',
            'j5'
        ];
    it('Should return an random item from an array', function() {
        expect(utils.getRandom(numArr)).to.be.within(1, 10);
        expect(utils.getRandom([1,2])).to.be.an('number');
    });

    it('Should generate an random number between range', function() {
        expect(utils.random(1, 10)).to.be.within(1, 10);
        expect(utils.random(1, 10)).to.be.an('number');
    });

    it('Should translate a input string into array', function() {
        console.log(cmds[0]);
        expect(utils.translateInput(cmds[0])).to.be.an('array');
        expect(utils.translateInput(cmds[0]).length).to.be(2);
    });


    it('Should validate input', function() {
        expect(utils.isValidCmd(utils.translateInput(cmds[0]))).to.be(false);
        expect(utils.isValidCmd(utils.translateInput(cmds[1]))).to.be(false);
        expect(utils.isValidCmd(utils.translateInput(cmds[2]))).to.be(true);
        expect(utils.isValidCmd(utils.translateInput(cmds[3]))).to.be(true);
        expect(utils.isValidCmd(utils.translateInput(cmds[4]))).to.be(false);
        expect(utils.isValidCmd(utils.translateInput(cmds[5]))).to.be(true);
        expect(utils.isValidCmd(utils.translateInput(cmds[6]))).to.be(false);
        expect(utils.isValidCmd(utils.translateInput(cmds[7]))).to.be(true);
    });

    it('Should convert the command', function() {
        expect(utils.convertCmd(utils.translateInput(cmds[2]))).to.be.an('array');
        expect(utils.convertCmd(utils.translateInput(cmds[2]))[0]).to.be(0);
        expect(utils.convertCmd(utils.translateInput(cmds[2]))[1]).to.be(9);
        expect(utils.convertCmd(utils.translateInput(cmds[7]))[0]).to.be(9);
        expect(utils.convertCmd(utils.translateInput(cmds[7]))[1]).to.be(4);
    });
});

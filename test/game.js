'use strict';

var expect = require('expect.js'),
    Game = require('../src/game'),
    config = require('../src/config');

describe('Player testing', function() {
    var game;

    beforeEach(function() {
        game = new Game();
    });

    it('Should create game instance', function() {
        expect(game.board.grid).to.be.an('array');
        expect(game.pieces).to.be.an('array');
        expect(game.player).to.be.an('object');
    });

    it('Should have pieces 3 pieces', function() {
        expect(game.pieces.length).to.be(3);
    });


    it('Should have pieces 1 BS and 2 destroyers', function() {
        var bs = 0, destroyers = 0;
        game.pieces.forEach(function(el) {
            if(el.val === config.HITPOINTS.BATTLESHIP) {
                bs++;
            }
            if(el.val === config.HITPOINTS.DESTROYER) {
                destroyers++;
            }
        });

        expect(bs).to.be(1);
        expect(destroyers).to.be(2);
        expect(game.pieces.length).to.be(3);
    });


    it('The board should have pieces', function() {
        game.pieces.forEach(function(el) {
            expect(el.coords.length).to.be.greaterThan(3);
        });
    });


    it('Should end the game', function() {
        game.pieces.forEach(function(el) {
            el.coords.forEach(function(coord) {
                coord[0] = coord[1] = config.HITPOINTS.HIT;
            });
            el.sink = true;
        });
        expect(game.areAllShipsSunk()).to.be(true);
    });


    it('Should continue the game', function() {
        game.pieces.forEach(function(el, i) {
            if(i === 2) {
                el.coords.forEach(function(coord) {
                    coord[0] = coord[1] = config.HITPOINTS.HIT;
                });
                el.sink = true;
            }
        });
        expect(game.areAllShipsSunk()).to.be(false);
    });

    it('Should fire and win the game', function() { 
        var win = false;
        game.board.grid.forEach(function(el, row) {
            el.forEach(function(el2, col) {
                win = !win ? game.fire([row, col]).win : win;
            });
        });
        expect(win).to.be(true);
    });
});

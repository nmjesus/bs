'use strict';

var expect = require('expect.js'),
    Piece = require('../src/piece');

describe('Piece testing', function() {
    it('Should create a piece', function() {
        var p1 = new Piece(10, 'horizontal', [0, 1], 1);
        expect(p1.size).to.be(10);
        expect(p1.orientation).to.be('horizontal');
        expect(p1.coords[0]).to.be(0);
        expect(p1.coords[1]).to.be(1);
        expect(p1.val).to.be(1);
    });
});

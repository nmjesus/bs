'use strict';

var expect = require('expect.js'),
    Board = require('../src/board'),
    Piece = require('../src/piece');

describe('Board testing', function() {
    var board;

    beforeEach(function() {
        board = new Board(10);
    });

    it('Should create a board 10x10', function() {
        board.grid.forEach(function(_cur) {
            expect(_cur.length).to.be(10);
        });
        expect(board.grid.length).to.be(10);
    });

    it('Should not add piece into board', function() {
        var p = new Piece(11, 'horizontal', [0, 0], 1);
        expect(board.putPiece(p)).to.be(false);
    });

    it('Should add piece into board', function() {
        var p = new Piece(10, 'horizontal', [0, 0], 1);
        expect(board.putPiece(p)).to.not.be(false);
    });

    it('Should add only one piece into board', function() {
        var p1 = new Piece(10, 'horizontal', [0, 0], 1);
        var p2 = new Piece(10, 'horizontal', [0, 0], 1);
        var p3 = new Piece(9, 'vertical', [1, 0], 1);
        var p4 = new Piece(9, 'vertical', [0, 1], 1);

        expect(board.putPiece(p1)).to.not.be(false);
        expect(board.putPiece(p2)).to.be(false);
        expect(board.putPiece(p3)).to.be(false);
        expect(board.putPiece(p4)).to.not.be(false);
    });

    it('Should add both pieces into board', function() {
        var p1 = new Piece(10, 'horizontal', [0, 0], 1);
        var p2 = new Piece(10, 'horizontal', [0, 1], 1);

        expect(board.putPiece(p1)).to.not.be(false);
        expect(board.putPiece(p2)).to.not.be(false);
    });

    it('Should return an array of coords', function() {
        var p1 = new Piece(10, 'horizontal', [0, 0], 1);
        expect(board.putPiece(p1).coords.length).to.be(10);
    });
});
